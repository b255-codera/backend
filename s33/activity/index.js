// 1.

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => json.map((el, index) => console.log(`${index}: ${el.title}`)));

// 2. 

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

// 3.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		userId: 1,
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 4.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated To Do List Item',
		userId: 1,
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 5. 

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 6. 

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
});