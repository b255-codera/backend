// Exponent Operator

const num = 2;
const getCube = num ** 3;

// Template Literals
console.log(`The cube of ${num} is ${getCube}`);



// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}`)


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach(number => console.log(number));

const reduceNumber = numbers.reduce((acc, current) => acc + current);
console.log(reduceNumber);
// Javascript Classes

class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog1 = new Dog("Frankie", 5, "Miniature Dachshund");
const myDog2 = new Dog("Cassie", 3, "Shih Tzu");
console.log(myDog1);
console.log(myDog2);

//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}
