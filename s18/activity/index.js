/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/
// 1. Create a function called addNum which will be able to add two numbers

function addNum(num1, num2) {
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
}
addNum(4, 3);

// 2. Create a function called subNum which will be able to substract two numbers

function subNum(num1, num2) {
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(num1 - num2);
}
subNum(10, 5);

// 3. Create a function called multiplyNum which will be able to multiply two numbers.

function multiplyNum(num1, num2) {
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;
}
let product = multiplyNum(2, 3);
console.log(product);

// 4. Create a function called divideNum which will be able to divide two numbers.

function divideNum(num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;
}
let quotient = divideNum(50, 5);
console.log(quotient);

// 5. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.

function getCircleArea(radius) {
	const pi = 3.1416;
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return pi * (radius ** 2);
}

let circleArea = getCircleArea(15);
console.log(circleArea);

// 6. Create a function called getAverage which will be able to get total average of four numbers

function getAverage(num1, num2, num3, num4) {
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", and " + num4 + ":");
	let sum = num1 + num2 + num3 + num4;
	return sum/4; 
}
let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);

// 7. Create a function called checkIfPassed
function checkIfPassed(score, total) {
	let isPassed = true;
	let percentage = (score/total) * 100;
	console.log("Is " + score + "/" + total + " a passing score?");
	
	if (percentage > 75) {
		return isPassed;
	} else {
		return !isPassed;
	}
}

let isPassingScore = checkIfPassed(38, 50);
console.log(isPassingScore);




//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
