// Screenshot1 - Use the count operator to count the total number of fruits on sale

db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $count: "fruitsOnSale"}
]);

// Screenshot2 - Use the count operator to count the total number of fruits with stock more than or equal to 20.

db.fruits.aggregate([
	{ $match: { stock : { $gte : 20}}},
	{ $count: "enoughStock"}
]);


// Screenshot3 - Use the average operator to get the average price of fruits onSale per supplier

db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: { _id: "$supplier_id", avg_price: { $avg: "$price"}}}
]);

// Screenshot 4 - User the max operator to get the highest price of fruit per supplier

db.fruits.aggregate([
	{ $group: { _id: "$supplier_id", max_price: { $max: "$price"}}}
]);

// Screenshot 5 - User the min operator to get the lowest price of fruit per supplier

db.fruits.aggregate([
	{ $group: { _id: "$supplier_id", min_price: { $min: "$price"}}}
]);
