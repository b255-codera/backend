const express = require('express');
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Home page
app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
});

// Signup (To add new user)
let users = [];
app.post("/signup", (req, res) => {
	console.log(req.body);

	// if contents of the "request.body" with the property "userName" and "password" is not empty
	if(req.body.userName !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.userName} successfully registered!`)
	} else {
		res.send("Please input BOTH username and password")
	}
});

// Get users
app.get("/users", (req, res) => {
	res.send(users);
});

// Delete user

app.delete("/delete-user", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++) {
		if(req.body.userName == users[i].userName){
			users.splice(i, 1);
			message = `User ${req.body.userName} has been deleted`;
			break;
		} else {
			message = "User does not exist."
		}		
	}

	if (message === undefined) {
		message = "No users found."
	} 

	res.send(message);
	
});



if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;