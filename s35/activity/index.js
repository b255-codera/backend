const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://jandy-255:admin123@zuitt-bootcamp.cvd9oso.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));


const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}).then((result, err) => {

		if((result != null && result.username == req.body.username) &&(req.body.password != null)){
			return res.send("Duplicate username found!");
		} else if (req.body.username != null && req.body.password != null) {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered!");
				}
			})
		} else {
			return res.send("BOTH username and password must be provided");
		}
	})
})


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;