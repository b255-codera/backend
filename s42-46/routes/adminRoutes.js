const express = require('express');
const { addAdmin, adminLogin, getAllAdmin} = require("../controllers/adminController");
const router = express.Router();
const auth = require("../auth");

// Create an admin
router.post("/signup", addAdmin);

// Admin login
router.post("/login", adminLogin);


module.exports = router;