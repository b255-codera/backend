const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required : [true, "Email is required"]
	},

	username: {
		type: String,
		unique: true,
		lowercase: true,
		required : [true, "Username is required"]
	},
	
	password: {
		type: String,
		minLength: 6,
		required : [true, "Password is required"]
	},

	isAdmin : {
		type : Boolean,
		default: true
	},
});

module.exports = mongoose.model("Admin", adminSchema);