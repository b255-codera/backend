const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required : [true, "Email is required"],
		trim: true,
		unique: true,
		lowercase: true
	},

	firstName: {
		type: String,
		required : [true, "First name is required"],
		trim: true
	},

	lastName: {
		type: String,
		required : [true, "Last name is required"],
		trim: true
	},
	
	password: {
		type: String,
		required : [true, "Password is required"],
		minLength: 6
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orderList: [
		{
			type: mongoose.Types.ObjectId,
			ref: "Order"
		}
	],

	// userCart: {
	// 	type: mongoose.Types.ObjectId,
	// 	ref: "Cart",
	// 	default: null
	// },

}, { timestamps: true });

module.exports = mongoose.model("User", userSchema);