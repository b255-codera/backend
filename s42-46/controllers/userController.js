const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.getAllUsers = async (req, res) => {
	try {
		const users = await User.find();

		if(!users) {
			return res.status(404).json({
				message: "No users found."
			});
		}

		return res.status(200).json({
			results: users.length, 
			users 
		});

	} catch (error) {
		return res.status(500).json({
			message: "An unexpected error occured."
		});
	}	
}

module.exports.getUser = async (req, res) => {

	try {
		const user = await User.findById(req.params.id);

		if(!user) {
			return res.status(404).json({
				message: "No user found."
			});
		}

		return res.status(200).json({
			user
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occured."
		});
	}
}

module.exports.getProfile = async (req, res, data) => {
	try {
		const user = await User.findById(data.userId);

		if(!user) {
			return res.status(404).send({
				message: "No user found."
			});
		}

		return res.status(200).send({
			user
		});

	} catch(error) {
		console.log(error);
		return res.status(500).send({
			message: "An unexpected error occured."
		});
	}
}

module.exports.registerUser = async (req, res) => {
	const { email, firstName, lastName, password } = req.body;

	if(!email || !firstName || !lastName || !password) 
	{
		return res.status(422).send({
			message: "Please fill out all the required fields!"
		});
	}

	const hashedPassword = bcrypt.hashSync(password, 10);

	try {
		// Check if the user already exists
		const existingUser = await User.findOne({ email });

		if(existingUser) {
			return res.status(409).send({ 
				message: "This email address is already associated with an account. Please login instead." 
			});
		}

		// Create a New User
		let user = new User({ 
			email, 
			firstName, 
			lastName, 
			password: hashedPassword 
		});

		user = await user.save();
		return res.status(201).send({
			message: "Account registration successful.", 
			user 
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).send({
			message: "An unexpected error occured."
		});
	}
};

module.exports.loginUser = async (req, res) => {
	const { email, password } = req.body;

	if (!email || !password) {
		return res.status(422).send({ 
			message: "Please enter your email/password." 
		});
	}

	try {
		const existingUser = await User.findOne({ email });

		if (!existingUser) {
			return res.status(400).send({ 
				message: "Incorrect email or password." 
			});
		}
	
		const isPasswordCorrect = await bcrypt.compare(password, existingUser.password);

		if (!isPasswordCorrect) {
			return res.status(400).send({ 
				message: "Incorrect email or password." 
			});
		}

		const accessToken = auth.createAccessToken(existingUser);
		return res.status(200).send({ 
			message: "Login successful", 
			accessToken 
		});

	} catch (error) {
		console.log(error);
		return res.status(500).send({ 
			message: "An unexpected error occured." 
		});
	}
};


module.exports.updateUser = async (req, res, userId) => {
	
	if(req.body.password) {
		req.body.password = bcrypt.hashSync(req.body.password, 10);
	}
	
	try {
		const user = await User.findByIdAndUpdate(req.params.id, 
			{
				$set: req.body
			}, 
			{
				new: true
			}
		);

		if (!user) {
			return res.status(404).json({ 
				message: "User not found!"
			});
		}

		if(userId === req.params.id) {
			return res.status(200).json({ 
				message: "User has been updated successfully."
			});
		}

		return res.status(401).json({
			message: "Authentication Failed!"
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occured."
		});
	}
};

module.exports.deleteUser = async (req, res) => {
	try {
		const user = await User.findByIdAndRemove(req.params.id);

		if (!user) {
			return res.status(404).json({ 
				message: "User does not exist!"
			});
		}

		return res.status(200).json({ 
			message: "User has been deleted successfully"
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occured."
		});
	}
};

module.exports.changeToAdmin = async (req, res) => {
	
	try {
		const user = await User.findByIdAndUpdate(req.params.id, 
			{
				$set: {
					isAdmin: true
				}
			},

			{
				new: true
			}
		)

		if(!user) {
			return res.status.json({
				message: "User not found."
			});
		}

		return res.status(200).json({
			user
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

