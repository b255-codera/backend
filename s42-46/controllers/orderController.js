const mongoose = require('mongoose');
const Order = require("../models/Order");
const User = require("../models/User");

module.exports.createOrder = async (req, res, userId) => {
	
	try {
		let newOrder = new Order({
			userId: userId,
			products: req.body.products,
			totalAmount: req.body.totalAmount,
			address: req.body.address,
			mobileNo: req.body.mobileNo
		});

		newOrder = await newOrder.save();
		if(!newOrder) {
			return res.status(422).json({
				message: "Unable to create order. Please try again!"
			});
		}

		let existingUser = await User.findById(userId);

		// Storing the Order ID to the user's orderList
		const session = await mongoose.startSession();
		session.startTransaction();
		existingUser.orderList.push(newOrder);
		await existingUser.save({session});
		await newOrder.save({session});
		session.commitTransaction();

		return res.status(201).json({
			message: "Success!",
			order: newOrder
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.cancelOrder = async (req, res) => {
	try {
		const order = await Order.findOneAndRemove({ 
			_id: req.params.orderId,
			status: "pending"
		}).populate("userId");

		if(!order) {
			return res.status(400).json({
				message: "Sorry! You can no longer cancel this order."
			});
		}

		const session = await mongoose.startSession();
		session.startTransaction();
		await order.userId.orderList.pull(order);
		await order.userId.save({ session });
		session.commitTransaction();

		return res.status(200).json({
			message: "Order has been cancelled successfully.",
			order
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.getUserOrders = async (req, res) => {
	try {
		const orders = await Order.find({userId: req.params.userId});

		if(!orders) {
			return res.status(404).json({
				message: "You haven't placed any orders yet."
			});
		}

		return res.status(200).json({
			results: orders.length,
			orders
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.getAllOrders = async (req, res) => {
	try {
		const orders = await Order.find();

		if(!orders) {
			return res.status(404).json({
				message: "No orders found."
			});
		}

		return res.status(200).json({
			results: orders.length,
			orders
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.updateOrderStatus = async (req, res) => {
	const { status } = req.body;
	const id = req.params.orderId;
	try {
		const order = await Order.findByIdAndUpdate(id,
			{
				$set: req.body
			},

			{
				new: true
			}
		)

		if(!order) {
			return res.status(404).json({
				message: "Order not found."
			});
		}

		return res.status(200).json({
			message: "Success!",
			order
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}