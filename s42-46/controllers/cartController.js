const Cart = require("../models/Cart");
const Product = require("../models/Product");
const User = require("../models/User");
const mongoose = require('mongoose');

module.exports.addProductToCart = async (req, res, userId) => {
	let existingUserCart;

	try {

		const isProductActive = await Product.findOne({ _id: req.body.productId, isActive:false});
		
		if(isProductActive) {
			return res.status(422).json({
				message: "The product is currently unavailable."
			});
		}

		existingUserCart = await Cart.findOne({ userId: userId });
		if(existingUserCart) {

			const isProductAdded = await existingUserCart.products.find(c => c.productId == req.body.productId);

			if(isProductAdded) {
				await Cart.findOneAndUpdate({ userId: userId, "products.productId": req.body.productId}, {
					$set: {
						"products.$": {
							...req.body,
							quantity: isProductAdded.quantity + req.body.quantity,
							subtotal: isProductAdded.price * (isProductAdded.quantity + req.body.quantity)
						}		
					}
				});
				
				return res.status(200).json({
					message: "Product has been added successfully to your cart."
				});
			}

			existingUserCart = await Cart.findOneAndUpdate({ userId: userId  }, {
				$push: {
					"products": {
						...req.body,
						subtotal: req.body.price * req.body.quantity
					}
				}
			});

			return res.status(200).json({
				message: "Product has been added successfully to your cart."
			});
		}

		let cart = new Cart({
			userId: userId,
			products: {
				...req.body,
				subtotal: req.body.price * req.body.quantity
			}
		});

		cart = await cart.save();

		if(!cart) {
			return res.status(422).json({
				message: "Unable to process the request."
			});
		}

		// let existingUser = await User.findById(userId);

		// // Storing the Order ID to the user's orderList
		// const session = await mongoose.startSession();
		// session.startTransaction();
		// existingUser.userCart = cart;
		// await existingUser.save({session});
		// await cart.save({session});
		// session.commitTransaction();

		return res.status(200).json({
			message: "Product has been added successfully to your cart."
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.updateCartQuantity = async (req, res) => {
	const userId = req.params.userId;

	try {
		const updatedCart = await Cart.findOneAndUpdate(
			{ 
				userId: userId, 
				"products.productId": req.body.productId
			},
			{
				$set: {
					"products.$.quantity": req.body.quantity,
					"products.$.subtotal": req.body.quantity * req.body.price
				}
			},
			{new:true}
		);
		return res.status(200).json({
			message: "Updated Successfully.",
			cart: updatedCart
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.removeProductsFromCart = async (req, res) => {
	const userId = req.params.userId;

	try {
		const userCart = await Cart.findOneAndUpdate(
			{ 
				userId: userId, 
				"products.productId": req.body.productId 
			},
			{
				$pull: {
					products: {
						productId: req.body.productId
					}
				}
			}
			);
		const count = userCart.products.length;

		if(count == 1) {
			await Cart.findOneAndRemove({userId: userId});
			return res.status(200).json({
				message: "Your cart is empty."
			});
		}

		return res.status(200).json({
			message: "Product has been deleted from your cart."
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.getCart = async (req, res) => {
	const userId = req.params.userId;
	try {
		const cart = await Cart.findOne({userId});

		if(!cart) {
			return res.status(404).json({
				message: "You cart is empty."
			});
		}

		const count = cart.products.length;

		if(count == 0) {
			await Cart.findByIdAndRemove(userId);
			return res.status(200).json({
				message: "Your cart is empty."
			});
		}

		return res.status(200).json({
			cart
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.getTotalAmount = async (req, res) => {
	try {
		const userCart = await Cart.findOne({userId: req.params.userId});
		if(!userCart) {
			return res.status(404).json({
				message: "No Cart found for this user."
			});
		}
		const getTotalAmount = userCart.products.reduce((total, item) => total + item.subtotal, 0)
		return res.status(200).json({
			total: getTotalAmount
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}