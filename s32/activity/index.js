const http = require("http");
const port = 4000;

const server = http.createServer((req, res) => {
	const path = req.url;

	if(path == '/' && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to Booking System');
	}

	if(path == '/profile' && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to your profile!');
	}

	if(path == '/courses' && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end(`Here's our courses available`);
	}

	if(path == '/addcourse' && req.method == "POST") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Add a course to our resources");
	}

	if(path == '/updatecourse' && req.method == "PUT") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Update a course to our resources");
	}

	if(path == '/archivecourses' && req.method == "DELETE") {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Archive courses to our resources");
	}

});

server.listen(port, () => {
	console.log(`Server running at localhost: ${port}`);
});