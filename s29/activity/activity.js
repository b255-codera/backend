// Screenshot1 - Find users with letter s in their first name or d in their last name

db.users.find(
	{ 
		$or: [
		{
			firstName: {$regex: 's', $options: '$i'}
		}, 

		{
			lastName: {$regex: 'd', $options: '$i'}
		}
		]}, 
		{
			firstName: 1, 
			lastName: 1, 
			_id: 0
		}

);

// Screenshot2 - Find users who have none in the company property and their age is greater than or equal to 70

db.users.find({$and: [{company: "none"}, {age: {$gte: 70}}]});

// Screenshot3 - Find users with the letter e in their first name and has an age of less than or equal to 30

db.users.find({$and: [{firstName: {$regex: 'e'}}, {age: {$lte: 30}}]});